Knightfall
===================

Knightfall, a package of Java utility classes for common tasks in every Java application development.

License
-------
This code is under the [Apache Licence v2](https://www.apache.org/licenses/LICENSE-2.0).

See the `NOTICE.txt` file for required notices and attributions.